#ifndef __DEFAULT_ALGORITHM_RESOLVER_H
#define __DEFAULT_ALGORITHM_RESOLVER_H

#include "IAlgorithmResolver.h"
#include "InsertionSort.h"

class DefaultAlgorithmResolver : public IAlgorithmResolver
{
public:
	ISortingAlgorithm * resolve(std::vector<int> & input) override;
};

#endif
