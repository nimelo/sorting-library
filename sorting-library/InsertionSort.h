#ifndef __INSERTION_SORT
#define __INSERTION_SORT

#include "ISortingAlgorithm.h"

class InsertionSort : public ISortingAlgorithm
{
public:
	explicit InsertionSort(std::vector<int> & input);
	void sort() override;
};

#endif

