#ifndef __QUICK_SORT
#define __QUICK_SORT

#include "ISortingAlgorithm.h"

class QuickSort : public ISortingAlgorithm
{
	int partition(const int left, const int right);
	void quicksort(const int left, const int right);
public:
	explicit QuickSort(std::vector<int> & input);
	void sort() override;
};

#endif
