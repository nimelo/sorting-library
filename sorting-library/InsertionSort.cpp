#include "InsertionSort.h"

InsertionSort::InsertionSort(std::vector<int> & input)
	: ISortingAlgorithm(input)
{
}

void InsertionSort::sort()
{
	for (size_t i = 0; i < input->size(); i++)
	{
		size_t k = i;
		while (k > 0 && (*input)[k] < (*input)[k-1])
		{
			swap(k, k - 1);
			k--;
		}
	}
}
