#ifndef __ARRAY_SORTER_H
#define __ARRAY_SORTER_H

#include "IAlgorithmResolver.h"
#include "DefaultAlgorithmResolver.h"
#include <vector>

class ArraySorter
{
	IAlgorithmResolver * _resolver;
public:
	explicit ArraySorter(IAlgorithmResolver * resolver);
	ArraySorter();
	~ArraySorter();

	void sort(std::vector<int> & input) const;
	void sort(std::vector<int> & input, std::vector<int> & auxArray) const;
};

#endif

