#include "QuickSort.h"

QuickSort::QuickSort(std::vector<int> & input)
	: ISortingAlgorithm(input)
{
}

int QuickSort::partition(const int left, const int right)
{
	const int mid = left + (right - left) / 2;
	const int pivot = (*input)[mid];
	swap(mid, left);
	int i = left + 1;
	int j = right;
	while (i <= j) 
	{
		while (i <= j && (*input)[i] <= pivot) i++;
		while (i <= j && (*input)[j] > pivot) j--;
		if (i < j) 
		{
			swap(i, j);
		}
	}
	swap(i - 1, left);
	return i - 1;
}

void QuickSort::quicksort(const int left, const int right)
{
	if (left >= right) return;
	int part = partition(left, right);
	quicksort(left, part - 1);
	quicksort(part + 1, right);
}

void QuickSort::sort()
{
	quicksort(0, input->size() - 1);
}
