#ifndef __I_ALGORITHM_RESOLVER
#define __I_ALGORITHM_RESOLVER

#include <vector>
#include "ISortingAlgorithm.h"

class IAlgorithmResolver
{
public:
	virtual ISortingAlgorithm * resolve(std::vector<int> & input) = 0;
};

#endif

