#include "DefaultAlgorithmResolver.h"
#include "ISortingAlgorithm.h"
#include "QuickSort.h"
#include <vector>

ISortingAlgorithm * DefaultAlgorithmResolver::resolve(std::vector<int> & input)
{
	const size_t INSERTION_SORT_THRESHOLD = 16;
	if (input.size() < INSERTION_SORT_THRESHOLD)
	{
		return new InsertionSort(input);
	}
	return new QuickSort(input);
}
