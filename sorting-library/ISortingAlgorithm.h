#ifndef __I_SORTING_ALGORITHM
#define __I_SORTING_ALGORITHM

#include <vector>
#include <numeric>

class ISortingAlgorithm
{
protected:
	std::vector<int> * input;
	std::vector<int> aux;

	void swap(int idx1, int idx2)
	{
		std::swap((*input)[idx1], (*input)[idx2]);
		std::swap(aux[idx1], aux[idx2]);
	}
public:
	explicit ISortingAlgorithm(std::vector<int> & input)
	{
		this->input = &input;
		this->aux = std::vector<int>(input.size());
		std::iota(aux.begin(), aux.end(), 0);
	}

	std::vector<int> getAux() const
	{
		return aux;
	}

	virtual void sort() = 0;
};

#endif
