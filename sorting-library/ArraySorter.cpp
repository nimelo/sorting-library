#include "ArraySorter.h"

ArraySorter::ArraySorter()
{
	this->_resolver = new DefaultAlgorithmResolver();
}

ArraySorter::ArraySorter(IAlgorithmResolver * resolver)
{
	this->_resolver = resolver;
}

ArraySorter::~ArraySorter()
{
	delete this->_resolver;
}

void ArraySorter::sort(std::vector<int> & input) const
{
	ISortingAlgorithm * algorithm = this->_resolver->resolve(input);
	algorithm->sort();
	delete algorithm;
}

void ArraySorter::sort(std::vector<int> & input, std::vector<int> & auxArray) const
{
	ISortingAlgorithm * algorithm = this->_resolver->resolve(input);
	algorithm->sort();
	auxArray = algorithm->getAux();
	delete algorithm;
}
