#pragma once
#include "ISortingTest.h"

class SortingPerformanceTest :
	public ISortingTest
{
	int processorL1CacheCount;
	int processorL1CacheSize;
	int processorL2CacheCount;
	int processorL2CacheSize;
	int processorL3CacheCount;
	int processorL3CacheSize;

public:
	SortingPerformanceTest();
	~SortingPerformanceTest();

	int getProcessorL1CacheSize() const { return processorL1CacheSize / processorL1CacheCount; }
	int getProcessorL2CacheSize() const { return processorL2CacheSize / processorL2CacheCount; }
	int getProcessorL3CacheSize() const { return processorL3CacheSize / processorL3CacheCount; }

	static unsigned int getSeedForRandom() { return 1; }

	static vec getRandomVector(int size);
};
