#pragma once
#include <gtest/gtest.h>
#include <ArraySorter.h>
#include <chrono>

class ISortingTest :
	public ::testing::Test
{
protected:
	typedef std::vector<int> vec;
	ArraySorter * sorter;

	virtual void SetUp() override
	{
		IAlgorithmResolver * resolver = new DefaultAlgorithmResolver();
		sorter = new ArraySorter(resolver);
	}

	virtual void TearDown() override
	{
		delete sorter;
	}

	long long measuredSort(vec &v) const
	{
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		sorter->sort(v);
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
		return microseconds;
	}
};
