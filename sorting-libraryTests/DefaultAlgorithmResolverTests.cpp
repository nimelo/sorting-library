#include <gtest/gtest.h>

#include "DefaultAlgorithmResolver.h"
#include <QuickSort.h>

TEST(DefaultAlgorithmResolverTest, ShouldReturnQuickSortAlgorithm)
{
	std::vector<int> v(16);
	DefaultAlgorithmResolver resolver;
	ISortingAlgorithm * algorithm = resolver.resolve(v);

	EXPECT_EQ(typeid(QuickSort), typeid(*algorithm));

	delete algorithm;
}

TEST(DefaultAlgorithmResolverTest, ShouldReturnInsertionSortAlgorithm)
{
	std::vector<int> v(15);
	DefaultAlgorithmResolver resolver;
	ISortingAlgorithm * algorithm = resolver.resolve(v);

	EXPECT_EQ(typeid(InsertionSort), typeid(*algorithm));

	delete algorithm;
}
