#include "SortingPerformanceTest.h"

#include <gtest/gtest.h>
#include <iostream>

TEST_F(SortingPerformanceTest, VectorOfLengthLessThanL1CacheShouldBeSorted)
{
	int itemCount = getProcessorL1CacheSize() / sizeof(int) / 2;
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;
	
	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthEqualToL1CacheShouldBeSorted)
{
	int itemCount = getProcessorL1CacheSize() / sizeof(int);
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthLessThanL2CacheShouldBeSorted)
{
	int itemCount = getProcessorL2CacheSize() / sizeof(int) / 2;
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthEqualToL2CacheShouldBeSorted)
{
	int itemCount = getProcessorL2CacheSize() / sizeof(int);
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthLessThanL3CacheShouldBeSorted)
{
	int itemCount = getProcessorL3CacheSize() / sizeof(int) / 2;
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthEqualToL3CacheShouldBeSorted)
{
	int itemCount = getProcessorL3CacheSize() / sizeof(int);
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VectorOfLengthGreaterThanL3CacheShouldBeSorted)
{
	int itemCount = getProcessorL3CacheSize() / sizeof(int) * 2;
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}

TEST_F(SortingPerformanceTest, VeryLongVectorShouldBeSorted)
{
	int itemCount = getProcessorL3CacheSize() / sizeof(int) * 100;
	vec v = getRandomVector(itemCount);

	long long microseconds = measuredSort(v);
	std::cout << "\t\tItems: " << itemCount << "\tSorting time: " << microseconds << "us" << std::endl;

	for (int i = 0; i < v.size() - 1; i++)
	{
		bool isNextElementGreaterOrEqual = v[i] <= v[i + 1];
		EXPECT_TRUE(isNextElementGreaterOrEqual) << "Elements are not sorted: " << v[i] << " > " << v[i + 1];
	}
}
