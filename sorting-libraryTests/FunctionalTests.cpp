#include "SortingFunctionalTest.h"

#include <gtest/gtest.h>

TEST_F(SortingFunctionalTest, SizeBeforeAndAfterSortingMatches)
{
	vec original = { 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vec toSort = vec(original);

	sorter->sort(toSort);

	ASSERT_EQ(original.size(), toSort.size()) << "Vector after sorting has different size.";
}

TEST_F(SortingFunctionalTest, ItemsBeforeAndAfterSortingMatches)
{
	vec original = { 0, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vec toSort = vec(original);

	sorter->sort(toSort);
	for (int i = original.size() - 1; i >= 0; i--)
	{
		bool found = false;
		for (int j = i; j >= 0 && !found; j--)
		{
			if (original[i] == toSort[j])
			{
				found = true;
				std::swap(toSort[i], toSort[j]);
			}
		}
		EXPECT_TRUE(found) << "Missing element in sorted vector: " << original[i];
	}
}

TEST_F(SortingFunctionalTest, ZeroLengthVectorShouldBeAccepted)
{
	vec original = {};
	vec toSort = vec(original);

	sorter->sort(toSort);

	ASSERT_EQ(toSort.size(), original.size()) << "Vector after sorting has different size.";
}

TEST_F(SortingFunctionalTest, ItemsWithCountLessThan16ShouldBeSorted)
{
	vec original = { 4, 10, 5, 11, 0, 1, 8, 3, 6, 2, 7, 9 };
	vec toSort = vec(original);

	sorter->sort(toSort);

	for (int i = 0; i < toSort.size() - 1; i++)
	{
		bool condition = toSort[i] > toSort[i + 1];
		EXPECT_FALSE(condition) << "Elements are not sorted: " << toSort[i] << " > " << toSort[i + 1];
	}
}

TEST_F(SortingFunctionalTest, ItemsWithCountMoreThan16ShouldBeSorted)
{
	vec original = { 22, -8, 4, 10, 5, -99, 11, 0, 1, 1245, 8, 3, 6, -9823, 2, 7, 1111, -239, 8856, 42374 };
	vec toSort = vec(original);

	sorter->sort(toSort);

	for (int i = 0; i < toSort.size() - 1; i++)
	{
		bool condition = toSort[i] > toSort[i + 1];
		EXPECT_FALSE(condition) << "Elements are not sorted: " << toSort[i] << " > " << toSort[i + 1];
	}
}

TEST_F(SortingFunctionalTest, AuxiliaryArrayShouldHaveTheSameSizeAsTheInputVector)
{
	vec original = { 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vec toSort = vec(original);
	vec auxArray;

	sorter->sort(toSort, auxArray);

	ASSERT_TRUE(auxArray.size() == toSort.size()) << "Auxiliary array and original array have different sizes.";
}

TEST_F(SortingFunctionalTest, AuxiliaryArrayShouldBeValid)
{
	vec original = { 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	vec toSort = vec(original);
	vec auxArray;
	vec validAuxArray = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1 };

	sorter->sort(toSort, auxArray);

	for (int i = 0; i < auxArray.size(); i++)
	{
		EXPECT_EQ(auxArray[i], validAuxArray[i]) << "Element in auxiliary array is incorrect: " << auxArray[i] << " -> " << validAuxArray[i];
	}
}
