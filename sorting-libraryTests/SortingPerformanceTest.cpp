#include "SortingPerformanceTest.h"

#include <windows.h>
#include <malloc.h>

SortingPerformanceTest::SortingPerformanceTest()
{
	processorL1CacheSize = processorL2CacheSize = processorL3CacheSize = 0;
	processorL1CacheCount = processorL2CacheCount = processorL3CacheCount = 0;
	DWORD buffer_size = 0;
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION * buffer;

	GetLogicalProcessorInformation(nullptr, &buffer_size);
	buffer = static_cast<SYSTEM_LOGICAL_PROCESSOR_INFORMATION *>(malloc(buffer_size));
	GetLogicalProcessorInformation(&buffer[0], &buffer_size);

	for (DWORD i = 0; i != buffer_size / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); i++)
	{
		if (buffer[i].Relationship == RelationCache)
		{
			switch (buffer[i].Cache.Level)
			{
			case 1:
				processorL1CacheCount++;
				processorL1CacheSize += buffer[i].Cache.Size;
				break;
			case 2:
				processorL2CacheCount++;
				processorL2CacheSize += buffer[i].Cache.Size;
				break;
			case 3:
				processorL3CacheCount++;
				processorL3CacheSize += buffer[i].Cache.Size;
				break;
			}
		}
	}
	free(buffer);
}


SortingPerformanceTest::~SortingPerformanceTest()
{
}

ISortingTest::vec SortingPerformanceTest::getRandomVector(int size)
{
	vec v = vec(size);
	srand(getSeedForRandom());
	for (int i = 0; i < size; i++)
		v[i] = rand();
	return v;
}
